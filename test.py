
class MyTest:
	class_attr = 1
	data = []
	def __init__(self,i):
		self.ins_attr = i
	def change(self,j):
		self.class_attr = j
	def change2(self,i):
		self.data = [i]

i = MyTest(20)
j = MyTest(-1)
i.class_attr = 99
i.change(500)
j.change(1000)

print MyTest.class_attr
print i.class_attr
print j.class_attr

i.data.append(1)

print i.data
print j.data

print MyTest.__dict__
print i.__dict__
print j.__dict__

i.change2(10)
j.change2(20)

print i.data
print j.data

print MyTest.__dict__
print i.__dict__
print j.__dict__

